# Create new ACM if no cert_arn is provided
resource "aws_acm_certificate" "cert" {
    count = var.loadbalancer_certificate_arn == "" ? 1 : 0

    domain_name       = "*.${var.domain}"
    validation_method = "DNS"

    subject_alternative_names = var.subject_alternative_names

    tags = {
        Environment = var.environment
    }

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_acm_certificate_validation" "cert" {
    certificate_arn = var.loadbalancer_certificate_arn == "" ? aws_acm_certificate.cert[0].arn : var.loadbalancer_certificate_arn
//     validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
}
