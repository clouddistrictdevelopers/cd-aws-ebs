## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_acm_certificate_validation.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation) | resource |
| [aws_elastic_beanstalk_application.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/elastic_beanstalk_application) | resource |
| [aws_elastic_beanstalk_environment.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/elastic_beanstalk_environment) | resource |
| [aws_iam_instance_profile.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.web_tier](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [random_id.ec2_suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_id.role_suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [aws_iam_policy_document.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_port"></a> [application\_port](#input\_application\_port) | Port application is listening on | `number` | `80` | no |
| <a name="input_appversion_lifecycle_delete_source_from_s3"></a> [appversion\_lifecycle\_delete\_source\_from\_s3](#input\_appversion\_lifecycle\_delete\_source\_from\_s3) | Whether to delete application versions from S3 source | `bool` | `false` | no |
| <a name="input_appversion_lifecycle_max_count"></a> [appversion\_lifecycle\_max\_count](#input\_appversion\_lifecycle\_max\_count) | The max number of application versions to keep | `number` | `100` | no |
| <a name="input_appversion_lifecycle_service_role_arn"></a> [appversion\_lifecycle\_service\_role\_arn](#input\_appversion\_lifecycle\_service\_role\_arn) | The service role ARN to use for application version cleanup. If left empty, the `appversion_lifecycle` block will not be created | `string` | `""` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | Your root domain, e.g: example.dev. | `any` | n/a | yes |
| <a name="input_ebs_app_name"></a> [ebs\_app\_name](#input\_ebs\_app\_name) | Elastic Beanstalk application name | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment where app should be deployed like dev, preprod or prod. | `string` | `"development"` | no |
| <a name="input_http_listener_enabled"></a> [http\_listener\_enabled](#input\_http\_listener\_enabled) | Enable port 80 (http) | `bool` | `true` | no |
| <a name="input_loadbalancer_certificate_arn"></a> [loadbalancer\_certificate\_arn](#input\_loadbalancer\_certificate\_arn) | Load Balancer SSL certificate ARN. The certificate must be present in AWS Certificate Manager | `string` | `""` | no |
| <a name="input_loadbalancer_managed_security_group"></a> [loadbalancer\_managed\_security\_group](#input\_loadbalancer\_managed\_security\_group) | Load balancer managed security group | `string` | `""` | no |
| <a name="input_loadbalancer_security_groups"></a> [loadbalancer\_security\_groups](#input\_loadbalancer\_security\_groups) | Load balancer security groups | `list(string)` | `[]` | no |
| <a name="input_loadbalancer_ssl_policy"></a> [loadbalancer\_ssl\_policy](#input\_loadbalancer\_ssl\_policy) | Specify a security policy to apply to the listener. This option is only applicable to environments with an application load balancer | `string` | `""` | no |
| <a name="input_project"></a> [project](#input\_project) | Name of project your app belongs to. | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | AWS region to use. | `string` | `"eu-west-1"` | no |
| <a name="input_settings"></a> [settings](#input\_settings) | settings to apply to EBS | `list(map(any))` | `[]` | no |
| <a name="input_solution_stack_name"></a> [solution\_stack\_name](#input\_solution\_stack\_name) | Elastic Beanstalk stack, e.g. Docker, Go, Node, Java, IIS. For more info, see https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html | `string` | n/a | yes |
| <a name="input_subject_alternative_names"></a> [subject\_alternative\_names](#input\_subject\_alternative\_names) | A list of domains that should be SANs in the issued certificate | `list(string)` | `[]` | no |
| <a name="input_version_label"></a> [version\_label](#input\_version\_label) | Elastic Beanstalk Application version to deploy | `string` | `""` | no |
| <a name="input_wait_for_ready_timeout"></a> [wait\_for\_ready\_timeout](#input\_wait\_for\_ready\_timeout) | The maximum duration to wait for the Elastic Beanstalk Environment to be in a ready state before timing out | `string` | `"20m"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ebs_application_name"></a> [ebs\_application\_name](#output\_ebs\_application\_name) | Elastic Beanstalk Application name |
| <a name="output_ebs_environment_all_settings"></a> [ebs\_environment\_all\_settings](#output\_ebs\_environment\_all\_settings) | List of all option settings configured in the environment. These are a combination of default settings and their overrides from setting in the configuration |
| <a name="output_ebs_environment_application"></a> [ebs\_environment\_application](#output\_ebs\_environment\_application) | The Elastic Beanstalk Application specified for this environment |
| <a name="output_ebs_environment_autoscaling_groups"></a> [ebs\_environment\_autoscaling\_groups](#output\_ebs\_environment\_autoscaling\_groups) | The autoscaling groups used by this environment |
| <a name="output_ebs_environment_endpoint"></a> [ebs\_environment\_endpoint](#output\_ebs\_environment\_endpoint) | Fully qualified DNS name for the environment |
| <a name="output_ebs_environment_id"></a> [ebs\_environment\_id](#output\_ebs\_environment\_id) | ID of the Elastic Beanstalk environment |
| <a name="output_ebs_environment_instances"></a> [ebs\_environment\_instances](#output\_ebs\_environment\_instances) | Instances used by this environment |
| <a name="output_ebs_environment_launch_configurations"></a> [ebs\_environment\_launch\_configurations](#output\_ebs\_environment\_launch\_configurations) | Launch configurations in use by this environment |
| <a name="output_ebs_environment_listener_certificate"></a> [ebs\_environment\_listener\_certificate](#output\_ebs\_environment\_listener\_certificate) | ARN of the 443 listener |
| <a name="output_ebs_environment_load_balancers"></a> [ebs\_environment\_load\_balancers](#output\_ebs\_environment\_load\_balancers) | Elastic Load Balancers in use by this environment |
| <a name="output_ebs_environment_queues"></a> [ebs\_environment\_queues](#output\_ebs\_environment\_queues) | SQS queues in use by this environment |
| <a name="output_ebs_environment_setting"></a> [ebs\_environment\_setting](#output\_ebs\_environment\_setting) | Settings specifically set for this environment |
| <a name="output_ebs_environment_tier"></a> [ebs\_environment\_tier](#output\_ebs\_environment\_tier) | The environment tier |
| <a name="output_ebs_environment_triggers"></a> [ebs\_environment\_triggers](#output\_ebs\_environment\_triggers) | Autoscaling triggers in use by this environment |
| <a name="output_ec2_instance_profile_role_name"></a> [ec2\_instance\_profile\_role\_name](#output\_ec2\_instance\_profile\_role\_name) | Instance IAM role name |
