variable "region" {
  //   type        = string
  description = "AWS region to use."
  default     = "eu-west-1"
}

variable "project" {
  type        = string
  description = "Name of project your app belongs to."
}

variable "ebs_app_name" {
  type        = string
  description = "Elastic Beanstalk application name"
}

variable "environment" {
  type        = string
  description = "Environment where app should be deployed like dev, preprod or prod."
  default     = "development"
}

variable "appversion_lifecycle_service_role_arn" {
  type        = string
  description = "The service role ARN to use for application version cleanup. If left empty, the `appversion_lifecycle` block will not be created"
  default     = ""
}

variable "appversion_lifecycle_max_count" {
  type        = number
  default     = 100
  description = "The max number of application versions to keep"
}

variable "appversion_lifecycle_delete_source_from_s3" {
  type        = bool
  default     = false
  description = "Whether to delete application versions from S3 source"
}

variable "solution_stack_name" {
  type        = string
  description = "Elastic Beanstalk stack, e.g. Docker, Go, Node, Java, IIS. For more info, see https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html"
}

variable "wait_for_ready_timeout" {
  type        = string
  default     = "20m"
  description = "The maximum duration to wait for the Elastic Beanstalk Environment to be in a ready state before timing out"
}


variable "version_label" {
  type        = string
  default     = ""
  description = "Elastic Beanstalk Application version to deploy"
}

# ELB
variable "domain" {
  description = "Your root domain, e.g: example.dev."
}
variable "subject_alternative_names" {
  type        = list(string)
  default     = []
  description = "A list of domains that should be SANs in the issued certificate"
}
variable "loadbalancer_certificate_arn" {
  type        = string
  default     = ""
  description = "Load Balancer SSL certificate ARN. The certificate must be present in AWS Certificate Manager"
}

variable "loadbalancer_ssl_policy" {
  type        = string
  default     = ""
  description = "Specify a security policy to apply to the listener. This option is only applicable to environments with an application load balancer"
}

variable "loadbalancer_security_groups" {
  type        = list(string)
  default     = []
  description = "Load balancer security groups"
}

variable "loadbalancer_managed_security_group" {
  type        = string
  default     = ""
  description = "Load balancer managed security group"
}

variable "http_listener_enabled" {
  type        = bool
  default     = true
  description = "Enable port 80 (http)"
}

variable "application_port" {
  type        = number
  default     = 80
  description = "Port application is listening on"
}

# variable "ssh_listener_enabled" {
#   type        = bool
#   default     = false
#   description = "Enable SSH port"
# }

# variable "ssh_listener_port" {
#   type        = number
#   default     = 22
#   description = "SSH port"
# }

variable "settings" {
  type        = list(map(any))
  default     = []
  description = "settings to apply to EBS"
}
