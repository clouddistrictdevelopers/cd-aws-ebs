# Create application container
resource "aws_elastic_beanstalk_application" "default" {
  name        = var.ebs_app_name
  description = "Managed by Terraform"

  dynamic "appversion_lifecycle" {
    for_each = var.appversion_lifecycle_service_role_arn != "" ? ["true"] : []
    content {
      service_role          = var.appversion_lifecycle_service_role_arn
      max_count             = var.appversion_lifecycle_max_count
      delete_source_from_s3 = var.appversion_lifecycle_delete_source_from_s3
    }
  }
}

resource "aws_elastic_beanstalk_environment" "default" {
  name        = "${var.project}-${var.environment}-${var.ebs_app_name}"
  application = var.ebs_app_name
  description = "Managed by Terraform"
  # "Node.js 12 running on 64bit Amazon Linux 2"
  solution_stack_name    = var.solution_stack_name
  wait_for_ready_timeout = var.wait_for_ready_timeout
  version_label          = var.version_label

  dynamic "setting" {
    for_each = [for s in var.settings : {
      name      = lookup(s, "name", "")
      namespace = lookup(s, "namespace", "")
      value     = lookup(s, "value", "")
      resource  = lookup(s, "resource", "")
    }]

    content {
      name      = setting.value.name
      namespace = setting.value.namespace
      value     = setting.value.value
      resource  = setting.value.resource
    }
  }

  setting {
    name      = "SSLCertificateId"
    namespace = "aws:elb:listener:443"
    resource  = ""
    value     = var.loadbalancer_certificate_arn == "" ? aws_acm_certificate.cert[0].arn : var.loadbalancer_certificate_arn
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.ec2.name
    resource  = ""
  }
}

# Move to IAM file
#
# EC2
#
data "aws_iam_policy_document" "ec2" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    effect = "Allow"
  }

  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "random_id" "role_suffix" {
  byte_length = 4
}
resource "aws_iam_role" "ec2" {
  name               = "${var.project}-${var.environment}-eb-ec2-${random_id.role_suffix.hex}"
  assume_role_policy = data.aws_iam_policy_document.ec2.json
}

resource "aws_iam_role_policy_attachment" "web_tier" {
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "random_id" "ec2_suffix" {
  byte_length = 4
}
resource "aws_iam_instance_profile" "ec2" {
  name = "${var.project}-${var.environment}-eb-ec2-${random_id.ec2_suffix.hex}"
  role = aws_iam_role.ec2.name
}
