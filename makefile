#crea los .md de los módulos terraform

DIR_TFDOCS = ../terraform-docs
COMMAND = $(DIR_TFDOCS)/terraform-docs

.PHONY : doc
doc:
	$(COMMAND) markdown . > README.md 
